const express = require("express");
const app = express();
const router = require("./routes");
const cors = require("cors");

app.use(cors("*"));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use("/", router);

app.use((err, req, res, next) => {
    return res.status(400).json(err.errors);
})

module.exports = app;

