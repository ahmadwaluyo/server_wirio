const { connection } = require("../config");
const { encrypt, decrypt } = require("../helpers/bcrypt");
const { v4 : uuidv4 } = require("uuid");

module.exports = {
    signUp: ({ username, email, password }) => {
        return new Promise((resolve, reject) => {
            connection.query(`INSERT INTO users(userId, username, password, email)
            VALUES ('${uuidv4()}', '${username}', '${encrypt(password)}', '${email}')
            RETURNING *`, (err, result) => {
                if (result) {
                    resolve(result)
                } else {
                    reject(new Error(err))
                }
            })
        })
    },
    signIn: ({ password, email }) => {
        return new Promise((resolve, reject) => {
            connection.query(`SELECT * FROM users WHERE email = '${email}'`, (err, result) => {
                let verifyPassword = decrypt(password, result.rows[0].password)
                if (verifyPassword) {
                    resolve(result)
                } else {
                    reject(new Error(err))
                }
            })
        })
    },
    findAllUser: () => {
        return new Promise((resolve, reject) => {
            connection.query(`SELECT * FROM users`, (err, result) => {
                if (result) {
                    resolve(result)
                } else {
                    reject(new Error(err))
                }
            })
        })
    },
    updateUser: (id, data) => {
        return new Promise((resolve, reject) => {
            connection.query(`SELECT * FROM users WHERE userId = '${id}'`, (err, result) => {
                if (result) {
                    console.log("resolve", result.rows)
                    const { username, email, address, phone } = result.rows[0];
                    connection.query(
                    `UPDATE users 
                    SET 
                    username = '${data.username || username}' 
                    , email = '${data.email || email}'
                    , address = '${data.address || address}'
                    , phone = '${data.phone || phone}'
                    WHERE userId = '${id}'
                    RETURNING *
                    `, (error, resultUpdate) => {
                        if (resultUpdate) {
                            resolve(resultUpdate)
                        } else {
                            console.log(error.message, "error")
                            reject(new Error(error))
                        }
                    })
                } else {
                    reject(new Error(err))
                }
            })
        })
    },
    deleteUser: ({ id }) => {
        return new Promise((resolve, reject) => {
            connection.query(`DELETE FROM users WHERE userId = '${id}'`, (err, result) => {
                if (result) {
                    resolve(result)
                } else {
                    reject(new Error(err))
                }
            })
        })
    }
}