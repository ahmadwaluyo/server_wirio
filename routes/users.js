const router = require("express").Router();
const { User } = require('../controllers');

router.get("/", User.findAllUser);
router.post("/signup", User.signUp);
router.post("/signin", User.signIn);
router.delete("/delete/:id", User.deleteUser);
router.patch("/update/:id", User.updateUser);

module.exports = router;