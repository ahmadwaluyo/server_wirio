const { users } = require("../models");
const { generateToken } = require("../helpers/jwt");

class User {
    static async signUp (req, res) {
        try {
            const { rows } = await users.signUp(req.body);
            res.json({
                message: 'success sign up !',
                data: rows.map(el => {
                    return {
                        "userid": el.userid,
                        "username": el.username,
                        "email": el.email,
                        "address": el.address,
                        "phone": el.phone
                    }
                })
            })
        } catch (err) {
            res.status(400).json({
                message: err.message,
            })
        }
    }

    static async signIn (req, res) {
        try {
            const data = await users.signIn(req.body);
            const { id, username, email, address, phone } = data.rows[0];
            const payload = {
                id,
                username,
                email,
                address,
                phone
            }
            res.json({
                message: 'success login !',
                data: {
                    token: generateToken(payload)
                }
            })
        } catch (err) {
            res.status(404).json({
                message: err.message,
            })
        }
    }

    static async findAllUser (req, res) {
        try {
            const { rows } = await users.findAllUser()
            res.json({
                message: 'success get data user !',
                data: rows.map(el => {
                    return {
                        "userid": el.userid,
                        "username": el.username,
                        "email": el.email,
                        "address": el.address,
                        "phone": el.phone
                    }
                })
            })
        } catch (err) {
            res.status(404).json({
                message: err.message,
            })
        }
    }

    static async updateUser (req, res) {
        const { id } = req.params;
        console.log(id, "id")
        console.log(req.body, "req.body")
        try {
            const { rows } = await users.updateUser(id, req.body);
            res.json({
                message: 'success update user !',
                data: rows.map(el => {
                    return {
                        "userid": el.userid,
                        "username": el.username,
                        "email": el.email,
                        "address": el.address,
                        "phone": el.phone
                    }
                })
            })
        } catch (err) {
            res.status(400).json({
                message: err.message,
            })
        }
    }

    static async deleteUser (req, res) {
        try {
            await users.deleteUser(req.params);
            res.json({
                message: 'success delete user !'
            })
        } catch(err) {
            res.status(500).json({
                message: err.message
            })
        }
    }
}

module.exports = User;